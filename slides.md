---
marp: true
class: invert 
theme: gaia
footer: Project
---

<!-- _class: lead -->
# Sort Visualizer 📊

`By: Juan Vergara & Jorge Loayza`
    

---

## `Methodology:` **Agile with Kanban method**

![bg contain right:45%](images/simple-kanban-board.jpg)

Priciples:

* VISUALIZE
* LIMIT (WIP)
* OPTIMIZE
* IMPROVE

**Tool:** GitLab

---

<!-- _class: lead-->

# `Roadmap`

`Five days` starting on November 23 and ending on Novermber 29
Working `8hr per day` and `4hr per person` with a total of 40 hr.

---
<!-- _class: lead -->
# `Technological Stack and Tools`

## **Angular**
## **GitLab**

___

<!-- _class: lead -->
# `Architectural Pattern`

### Frontend: Model-View-View-Model (MVVM)
**Using Separation of concerns & LIFT principles**

___


```
sort-visualizer/
...
├── src/
│   ├── app/
│   │   ├── app-routing.module.ts
│   │   ├── app.component.html
│   │   ├── app.component.scss
│   │   ├── app.component.spec.ts
│   │   ├── app.component.ts
│   │   ├── app.module.ts
├── ...
├── lib/
│   ├── sorter/
│   │   ├── bubblesort.ts
│   │   ├── heapsort.ts
│   │   ├── index.ts
│   │   ├── ...
│   ├── util/
│   │   ├── operation.ts
│   │   ├── sleep.ts
...

```

---

##### `Architecture:` SOFEA

![bg contain right:75%](images/sofea.png)

---

<!-- _class: lead -->

# `Self Evaluation:` Issues

---

![bg 100%](images/issues.png)

---

<!-- _class: lead -->

# `Self Evaluation:` Estimate

---

![bg 100%](images/estimate.png)

---
# `References`

- Filipova, O., & Vilão, R. (2018). Software Development From A to Z. Primera Edic. Berlín: Apress.
- Layton, M. C., Ostermiller, S. J., & Kynaston, D. J. (2020). Agile project management for dummies. John Wiley & Sons.
- https://codingsans.com/blog/kanban-in-software-development
- https://www.atlassian.com/agile/kanban
- http://bguiz.github.io/js-standards/angularjs/application-structure-lift-principle/
