FROM node:16.13.1 as build
WORKDIR /usr/local/app
COPY ./ /usr/local/app/
RUN npm install
RUN npm run build

FROM nginx:alpine as final
COPY --from=build /usr/local/app/dist/frontend/ /usr/share/nginx/html/
EXPOSE 80
