# Gitlab Runner

## Installation as a Container

1. Create the Docker volume:

```sh
$ docker volume create gitlab-runner-config
```

2. Start the GitLab Runner container using the volume we just created:

```sh
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    gitlab/gitlab-runner:alpine
```

### Registration

```sh
docker run --rm -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:alpine register \
  --non-interactive \
  --executor "docker" \
  --docker-image docker:19.03.12 \
  --url "https://gitlab.com/" \
  --registration-token "PROJECT_REGISTRATION_TOKEN" \
  --description "docker-runner" \
  --tag-list "docker,did" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected" \
  --docker-privileged
```

## Installation as a package



