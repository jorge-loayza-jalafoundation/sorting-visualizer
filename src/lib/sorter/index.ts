export * from './bubblesort';
export * from './heapsort';
export * from './mergesort';
export * from './quicksort';
export * from './selectionsort';


export interface Item {
    value: number;
    active: boolean;
    swapping: boolean;
    finalrun: boolean;
    pivot: boolean;
}

export enum SortingAlg {
    bubble = 0,
    merge = 1,
    heap = 2,
    quick = 3,
    selection = 4
}

export type SortComparator = (a: Item, b: Item) => boolean;
