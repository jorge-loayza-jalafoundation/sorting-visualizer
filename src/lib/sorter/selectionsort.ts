import { Item, SortComparator } from ".";
import { Delay, Cancellation } from '../util/operation';
import { sleep } from '../util/sleep';

function swap(array: Item[], indexA: number, indexB: number, delay: Delay) {
    var temp = array[indexA];
    array[indexA] = array[indexB];
    array[indexB] = temp;
}

export async function selectionSort(array: Item[], comparator: SortComparator, delay: Delay, stopping: Cancellation) {
    if (stopping.cancelRequested) return;
    for (let i = 0; i < array.length - 1; i++) {
        if (stopping.cancelRequested) break;
        var minIndex = i;

        for (let j = i + 1; j < array.length; j++) {
            if (array[j].value < array[minIndex].value) {
                minIndex = j;
            }
        }

        if (minIndex !== i) {
            let cur = array[i];
            let min = array[minIndex];
            cur.active = true;
            min.active = true;
            if (comparator(cur, min)) {
                await sleep(delay.delay);
                cur.swapping = true;
                min.swapping = true;
                await sleep(delay.delay);
                swap(array, i, minIndex, delay);
                cur.swapping = false;
                min.swapping = false;
            }
            await sleep(delay.delay);
            cur.active = false;
            min.active = false;
        }
    }
}

